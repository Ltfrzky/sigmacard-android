package com.sigmacard.data.repository.login

import com.sigmacard.base.model.BaseResponse
import com.sigmacard.data.DataResult
import com.sigmacard.data.model.User

interface LoginRepository {
    suspend fun login(username: String, password:String): DataResult<BaseResponse<User>>
}