package com.sigmacard.data.repository.login

import android.content.Context
import com.sigmacard.base.model.BaseResponse
import com.sigmacard.data.DataResult
import com.sigmacard.data.api.libs.ApiInterface
import com.sigmacard.data.api.request.LoginRequest
import com.sigmacard.data.db.dao.UserDao
import com.sigmacard.data.model.User
import com.sigmacard.util.ext.handleApiError
import com.sigmacard.util.ext.handleSuccess
import com.sigmacard.util.ext.isOnline
import com.sigmacard.util.ext.noNetworkConnectivityError
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception

class LoginRepositoryImpl(
    private val api: ApiInterface,
    private val context: Context,
    private val userDao: UserDao
): LoginRepository {

    override suspend fun login(username: String, password: String): DataResult<BaseResponse<User>> {
        if (isOnline(context)){
            return try {
                val response = api.login(LoginRequest(username, password))
                if (response.isSuccessful){
                    response.body()?.let {
                        withContext(Dispatchers.IO){
                            userDao.add(it.data)
                        }
                    }
                    handleSuccess(response)

                }else{
                    handleApiError(response)
                }
            }catch (e: Exception){
                DataResult.Error(e)
            }
        } else{
           return noNetworkConnectivityError()
        }
    }
}