package com.sigmacard.data.api.libs

import com.sigmacard.base.model.BaseResponse
import com.sigmacard.data.api.request.LoginRequest
import com.sigmacard.data.api.request.SignUpRequest
import com.sigmacard.data.model.User
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

/**
 *created by Lutfi Rizky Ramadhan on 08/05/20
 */

interface ApiInterface {
    @POST("api/login")
    suspend fun login(@Body loginRequest: LoginRequest): Response<BaseResponse<User>>

    @POST("api/register")
    suspend fun signUp(@Body signUpRequest: SignUpRequest): Response<BaseResponse<Unit>>
}