package com.sigmacard.data.api.request

import com.google.gson.annotations.SerializedName

/**
 *created by Lutfi Rizky Ramadhan on 12/05/20
 */

data class SignUpRequest(
    @SerializedName("name")
    var name: String,

    @SerializedName("email")
    var email: String,

    @SerializedName("phone")
    var phone: String,

    @SerializedName("password")
    var password: String
)