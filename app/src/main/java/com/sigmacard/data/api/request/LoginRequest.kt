package com.sigmacard.data.api.request

import com.google.gson.annotations.SerializedName

/**
 *created by Lutfi Rizky Ramadhan on 09/05/20
 */

data class LoginRequest(
    @SerializedName("emailOrPhone")
    var username: String,

    @SerializedName("password")
    var password: String
)