package com.sigmacard.view.activity.login

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sigmacard.base.model.BaseResponse
import com.sigmacard.data.DataResult
import com.sigmacard.data.model.User
import com.sigmacard.data.repository.login.LoginRepository
import kotlinx.coroutines.launch

class LoginViewModel(private val repository: LoginRepository): ViewModel() {

    val login = MutableLiveData<DataResult<BaseResponse<User>>>()

    fun login(username: String, password: String){
        viewModelScope.launch {
            val loginResponse = repository.login(username, password)
            login.value = loginResponse
        }
    }
}