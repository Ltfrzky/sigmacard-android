package com.sigmacard.view.activity.splash

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.sigmacard.R
import com.sigmacard.base.BaseActivity

class SplashActivity : BaseActivity() {

    override fun getLayoutResource(): Int {
        return R.layout.activity_splash
    }

    override fun initObserver() {
        TODO("Not yet implemented")
    }

    override fun initIntent() {
        TODO("Not yet implemented")
    }

    override fun initUI() {
        TODO("Not yet implemented")
    }

    override fun initAction() {
        TODO("Not yet implemented")
    }

    override fun initProcess() {
        TODO("Not yet implemented")
    }
}
