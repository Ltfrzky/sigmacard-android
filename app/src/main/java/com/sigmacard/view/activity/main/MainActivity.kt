package com.sigmacard.view.activity.main

import android.content.Context
import android.content.Intent
import android.view.MenuItem
import com.sigmacard.R
import com.sigmacard.base.BaseActivity
import com.sigmacard.view.fragment.history.HistoryFragment
import com.sigmacard.view.fragment.home.HomeFragment
import com.sigmacard.view.fragment.profile.ProfileFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar_main.*

class MainActivity : BaseActivity() {

    private val homeFragment = HomeFragment.newInstance()

    private val historyFragment = HistoryFragment.newInstance()

    private val profileFragment = ProfileFragment.newInstance()

    companion object {
        const val NFC_SCAN_REQUEST = 24
        fun start(context: Context) {
            val starter = Intent(context, MainActivity::class.java)
            context.startActivity(starter)
        }
    }

    override fun getLayoutResource(): Int {
        return R.layout.activity_main
    }

    override fun initObserver() {

    }

    override fun initIntent() {
//        TODO("Not yet implemented")
    }

    override fun initUI() {
        setupToolbar(toolbarApp, false)
        setupMain()
    }

    override fun initAction() {
//        btnNfc.setOnClickListener {
////            NfcActivity.start(this)
//            startActivityForResult(Intent(this, NfcActivity::class.java), NFC_SCAN_REQUEST)
//        }
        bnvMain.setOnNavigationItemSelectedListener(this::decideFragment)
    }

    override fun initProcess() {

    }

    private fun setupMain() {
        if (!homeFragment.isAdded) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.frmContentMain, homeFragment)
                .commit()
        }
    }

    private fun decideFragment(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menuHome -> if (!homeFragment.isAdded) {
                supportFragmentManager.beginTransaction()
                    .replace(R.id.frmContentMain, homeFragment)
                    .commit()
                return true
            }
            R.id.menuHistory -> if (!historyFragment.isAdded) {
                supportFragmentManager.beginTransaction()
                    .replace(R.id.frmContentMain, historyFragment)
                    .commit()
                return true
            }
            R.id.menuProfile -> if (!profileFragment.isAdded) {
                supportFragmentManager.beginTransaction()
                    .replace(R.id.frmContentMain, profileFragment)
                    .commit()
                return true
            }
        }
        return false
    }
}
