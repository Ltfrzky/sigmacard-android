package com.sigmacard.view.activity.signup

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.sigmacard.R
import com.sigmacard.base.BaseActivity

class SignUpActivity : BaseActivity() {

    override fun getLayoutResource(): Int {
        return R.layout.activity_sign_up
    }

    override fun initObserver() {
        TODO("Not yet implemented")
    }

    override fun initIntent() {
        TODO("Not yet implemented")
    }

    override fun initUI() {
        TODO("Not yet implemented")
    }

    override fun initAction() {
        TODO("Not yet implemented")
    }

    override fun initProcess() {
        TODO("Not yet implemented")
    }
}
