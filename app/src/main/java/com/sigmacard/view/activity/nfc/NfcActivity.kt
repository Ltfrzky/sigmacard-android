package com.sigmacard.view.activity.nfc

import android.app.Activity
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.nfc.NfcAdapter
import android.nfc.Tag
import android.nfc.tech.IsoDep
import android.provider.Settings.ACTION_NFC_SETTINGS
import android.util.Log
import android.widget.Toast
import com.sigmacard.R
import com.sigmacard.base.BaseActivity
import com.sigmacard.util.BundleKeys
import com.sigmacard.util.NfcUtils

class NfcActivity : BaseActivity(), NfcAdapter.ReaderCallback {

    private var nfcAdapter: NfcAdapter? = null

    // launch our application when a new Tag or Card will be scanned
    private var pendingIntent: PendingIntent? = null

    companion object {
        fun start(context: Context) {
            val starter = Intent(context, NfcActivity::class.java)
            context.startActivity(starter)
        }
    }

    override fun getLayoutResource(): Int {
        return R.layout.activity_nfc
    }

    override fun initObserver() {
//        TODO("Not yet implemented")
    }

    override fun initIntent() {
//        TODO("Not yet implemented")
    }

    override fun initUI() {
        nfcAdapter = NfcAdapter.getDefaultAdapter(this)

        if (nfcAdapter == null) {
            Toast.makeText(this, "No NFC", Toast.LENGTH_SHORT).show()
            finish()
            return
        }

        pendingIntent = PendingIntent.getActivity(
            this, 0,
            Intent(this, this.javaClass)
                .addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0
        )
    }

    override fun initAction() {
//        TODO("Not yet implemented")
    }

    override fun initProcess() {
//        TODO("Not yet implemented")
    }

    override fun onResume() {
        super.onResume()

        if (nfcAdapter != null) {
            if (!nfcAdapter!!.isEnabled) showNFCSettings()

            nfcAdapter?.enableReaderMode(this, this, NfcAdapter.FLAG_READER_NFC_A or NfcAdapter.FLAG_READER_SKIP_NDEF_CHECK, null)
        }
    }

    override fun onPause() {
        super.onPause()
        if (nfcAdapter != null) {
            nfcAdapter?.disableReaderMode(this)
        }
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        setIntent(intent)
    }

    private fun showNFCSettings() {
        Toast.makeText(this, "You need to enable NFC", Toast.LENGTH_SHORT).show()
        val intent = Intent(ACTION_NFC_SETTINGS)
        startActivity(intent)
    }

    private fun sendCardNumber(cardNumber: String) {
        val data = Intent().putExtra(BundleKeys.KEY_CARD_NUMBER, cardNumber)
        setResult(Activity.RESULT_OK, data)
        finish()
    }

    override fun onTagDiscovered(tag: Tag?) {
        val isoDep = IsoDep.get(tag)
        var cardNumber = ""
        isoDep.connect()
        try {
            var response =
                isoDep.transceive(NfcUtils.hexStringToByteArray("00A40400080000000000000001"))

            Log.d("NFC", "response -> $response")
            var responseHex = NfcUtils.toHex(response)
            Log.d("NFC", "responseHex -> $responseHex")

            response = isoDep.transceive(NfcUtils.hexStringToByteArray("00B300003F"))
            responseHex = NfcUtils.toHex(response)
            Log.d("NFC Card Info", "response -> $response")
            Log.d("NFC Card Info", "responseHex -> $responseHex")
            cardNumber = responseHex.substring(0, 16)
            Log.d("NFC Card Info", "Card Number -> $cardNumber")
        }catch (e: StringIndexOutOfBoundsException){
            e.printStackTrace()
        }
        sendCardNumber(cardNumber)

        isoDep.close()
    }
}
