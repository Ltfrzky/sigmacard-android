package com.sigmacard.view.activity.detailtransaction

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.sigmacard.R

class DetailTransactionActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_transaction)
    }
}
