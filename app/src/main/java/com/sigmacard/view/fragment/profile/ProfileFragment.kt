package com.sigmacard.view.fragment.profile

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.sigmacard.R
import com.sigmacard.base.BaseFragment

/**
 * A simple [Fragment] subclass.
 */
class ProfileFragment : BaseFragment() {

    companion object{
        fun newInstance(): ProfileFragment {

            val args: Bundle? = null

            val fragment = ProfileFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun getLayoutResource(): Int {
        return R.layout.fragment_profile
    }

    override fun initObserver() {
        TODO("Not yet implemented")
    }

    override fun initIntent(savedInstanceState: Bundle?) {
        TODO("Not yet implemented")
    }

    override fun initUI() {
        TODO("Not yet implemented")
    }

    override fun initAction() {
        TODO("Not yet implemented")
    }

    override fun initProcess() {
        TODO("Not yet implemented")
    }

}
