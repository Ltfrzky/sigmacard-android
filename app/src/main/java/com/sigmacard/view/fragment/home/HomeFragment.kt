package com.sigmacard.view.fragment.home

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.widget.Toast

import com.sigmacard.R
import com.sigmacard.base.BaseFragment
import com.sigmacard.util.BundleKeys
import com.sigmacard.view.activity.main.MainActivity
import com.sigmacard.view.activity.nfc.NfcActivity
import kotlinx.android.synthetic.main.fragment_home.*

/**
 * A simple [Fragment] subclass.
 */
class HomeFragment : BaseFragment() {

    companion object{
        const val NFC_SCAN_REQUEST = 24

        fun newInstance(): HomeFragment {

            val args: Bundle? = null

            val fragment = HomeFragment()
            fragment.arguments = args

            return fragment
        }
    }

    override fun getLayoutResource(): Int {
        return R.layout.fragment_home
    }

    override fun initObserver() {
//        TODO("Not yet implemented")
    }

    override fun initIntent(savedInstanceState: Bundle?) {
//        TODO("Not yet implemented")
    }

    override fun initUI() {
//        TODO("Not yet implemented")
    }

    override fun initAction() {
        fabNfc.setOnClickListener{
            startActivityForResult(Intent(activity, NfcActivity::class.java), NFC_SCAN_REQUEST)
        }
    }

    override fun initProcess() {
//        TODO("Not yet implemented")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == NFC_SCAN_REQUEST && resultCode == Activity.RESULT_OK && data != null) {
            val cardNumber = data.getStringExtra(BundleKeys.KEY_CARD_NUMBER)
            if(cardNumber?.length == 16){
                Toast.makeText(context, cardNumber, Toast.LENGTH_SHORT).show()
                edtCardNumber.setText(cardNumber)
            }else{
                Toast.makeText(context, "Failed To Get Card Number", Toast.LENGTH_SHORT).show()
            }
        } else {
            Toast.makeText(context, "Failed To Get Card Number", Toast.LENGTH_SHORT).show()
        }
    }
}
