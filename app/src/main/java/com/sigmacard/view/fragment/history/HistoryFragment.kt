package com.sigmacard.view.fragment.history

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.sigmacard.R
import com.sigmacard.base.BaseFragment

/**
 * A simple [Fragment] subclass.
 */
class HistoryFragment : BaseFragment() {

    companion object{
        fun newInstance(): HistoryFragment {

            val args: Bundle? = null

            val fragment = HistoryFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun getLayoutResource(): Int {
        return R.layout.fragment_history
    }

    override fun initObserver() {
//        TODO("Not yet implemented")
    }

    override fun initIntent(savedInstanceState: Bundle?) {
//        TODO("Not yet implemented")
    }

    override fun initUI() {
//        TODO("Not yet implemented")
    }

    override fun initAction() {
//        TODO("Not yet implemented")
    }

    override fun initProcess() {
//        TODO("Not yet implemented")
    }

}
