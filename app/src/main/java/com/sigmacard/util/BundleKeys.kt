package com.sigmacard.util

/**
 *created by Lutfi Rizky Ramadhan on 11/05/20
 */

object BundleKeys {
    const val KEY_CARD_NUMBER = "key_card_number"
    const val KEY_TOKEN = "key_token"
}