package com.sigmacard.base

import androidx.appcompat.widget.Toolbar

/**
 *created by Lutfi Rizky Ramadhan on 09/05/20
 */

interface BaseView {

    fun setupToolbar(toolbar: Toolbar?, isChild: Boolean)

    fun setupToolbar(toolbar: Toolbar?, title: String, isChild: Boolean)

    fun showMessage(message: String)

}