package com.sigmacard.base.di.featuremodule

import com.sigmacard.data.repository.login.LoginRepository
import com.sigmacard.data.repository.login.LoginRepositoryImpl
import com.sigmacard.view.activity.login.LoginViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val loginModule = module {

    single<LoginRepository> { LoginRepositoryImpl(get(), get(), get()) }

    viewModel { LoginViewModel(get()) }
}