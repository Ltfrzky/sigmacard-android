package com.sigmacard.base.di

import android.os.Bundle
import com.sigmacard.BuildConfig
import com.sigmacard.data.api.libs.ApiInterface
import com.sigmacard.data.api.libs.HeaderInterceptor
import com.sigmacard.data.pref.AppPreference
import com.sigmacard.util.BundleKeys
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


val networkModule = module {
    single { provideOkHttpClient(get(), get()) }
    single { provideLoggingInterceptor() }
    single { provideRetrofit(get()) }
    single { provideApi(get()) }
    single { provideHeaderInterceptor(get()) as HeaderInterceptor }
}

fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder().baseUrl(BuildConfig.BASE_URL).client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create()).build()
}

fun provideOkHttpClient(loggingInterceptor: HttpLoggingInterceptor, headerInterceptor: HeaderInterceptor): OkHttpClient {
    return OkHttpClient().newBuilder().addInterceptor(loggingInterceptor).addInterceptor(headerInterceptor).build()
}

fun provideHeaderInterceptor(appPreference: AppPreference): Interceptor{
    val headers = HashMap<String, String>()

    //define default headers here
    appPreference.getValueString(BundleKeys.KEY_TOKEN)?.let { headers.put("Authorization", it) }

    return HeaderInterceptor(headers, appPreference)
}

fun provideLoggingInterceptor(): HttpLoggingInterceptor {
    val logger = HttpLoggingInterceptor()
    logger.level = HttpLoggingInterceptor.Level.BODY
    return logger
}

fun provideApi(retrofit: Retrofit): ApiInterface = retrofit.create(ApiInterface::class.java)