package com.sigmacard.base.di

import com.sigmacard.data.db.AppDatabase
import org.koin.dsl.module

val dbModule = module {
    single { AppDatabase.getInstance(get()) }
    single { get<AppDatabase>().userDao() }
}