package com.sigmacard.base.di

import com.sigmacard.data.pref.AppPreference
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val prefModule = module {
    single { AppPreference(androidContext()) }
}