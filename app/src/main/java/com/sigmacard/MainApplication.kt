package com.sigmacard

import android.app.Application
import com.sigmacard.base.di.*
import com.sigmacard.base.di.featuremodule.loginModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

/**
 *created by Lutfi Rizky Ramadhan on 09/05/20
 */

class MainApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        // start Koin
        startKoin {
            androidContext(this@MainApplication)
            modules(listOf(dbModule, prefModule, networkModule, loginModule))
        }
    }
}